// console.log('Hello World');

const getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

// let home_number = 258;
// let street_name = "Washington Ave NW";
// let country = "California"
// let zip_code = 90011;

const address = ["258", "Washington Ave NW", "California", "90011"];
const [home_number, street_name, country_name, zip_code] = address
console.log(`I live at ${home_number} ${street_name} ${country_name} ${zip_code}`);

const animal = {
	name: "Lolong",
	kind: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in.",
}

const {name, kind, weight, measurement} = animal;
console.log(`${name} was a ${kind}. He weighed at ${weight} with a measurement of ${measurement}`);


const arr_numbers = [1, 2, 3, 4, 5];
arr_numbers.forEach((number) => console.log(number));

let iteration = 0;

let reducedArray = arr_numbers.reduce((x, y) => {

	return x + y;
})
console.log(reducedArray);

const dog = {
	name: "Frankie",
	age: 5,
	breed: "Miniature Dachshund"
}
console.log(dog);